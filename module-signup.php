<div class="widget-container signup">
	<div class="widget-subtitle">
		Signup today and receive the
	</div>
	<div class='widget-title'>
		Free 2014 US Property Investment Package
	</div>
	<div class="widget-body">
		<div class="widget-inner row-fluid">
			<form method="post" action="?investment_package=submit">
				<?php if(isset($_GET['arei_error'])){?>
					<div class="alert error">
						<?php echo "Data validation failed";?>
					</div>
				<?php }elseif(isset($_GET['arei_success'])) { ?>
					<div class="alert alert-success">
						<?php echo "We have recieved your request. Someone will respond to you ASAP.";?>	
					</div>
				<?php } ?>
				<input type="hidden" name="previous_url" value="<?php global $wp; echo home_url(add_query_arg(array(),$wp->request));?>">
				<label style="display:none;">First Name</label>
				<input type="text" class="input-full" name="first_name" placeholder="First Name"/>
				<label style="display:none;">Last Name</label>
				<input type="text" class="input-full" name="last_name" placeholder="Last Name"/>
				<label style="display:none;">Email</label>
				<input type="text" class="input-full" name="email" placeholder="Email"/>
				<label style="display:none;">Phone</label>
				<input type="text" class="input-full" name="phone" placeholder="Phone"/>
				<button style="" class="submit btn btn-medium btn-block btn-darkgray">Sign Up Now</button>
			</form>
		</div>
	</div>
</div>