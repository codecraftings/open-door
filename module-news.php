<?php
$items = wp_get_recent_posts(array('category' => 7,'numberposts'=>6, 'post_status' => 'publish'));
?>
<div class="widget-container news">
    <div class="widget-title">
        News Blog
    </div>
    <div class="widget-body">
        <div class="widget-body-inner">
            <ul id="news-accordion" class="news-list accordion">
                <?php foreach ($items as $item): ?>
                    <li class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" href="#post_<?= $item['ID'];?>" data-parent="news-accordion" data-toggle="collapse">
                                <img src="<?= arei_get_img($item['ID'], 60,60);?>"/>
                                <?= $item['post_title'] ?>
                            </a>
                        </div>
                        <div id="post_<?= $item['ID'];?>" class="accordion-body collapse">
                                <?= arei_post_summary($item['post_content']);?>
                                <a href="<?= get_permalink($item['ID']);?>">read more</a>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
